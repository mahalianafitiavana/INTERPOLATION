#include <stdlib.h>
#include <stdio.h>
void getData (int *dim, float ** xd, float ** yd, float ** a, float ** b, float ** s, float *** M , float **Y);
void displayResult ( float * x,int dim);
void problem (char * message);
float *newVect (int dim);
float **newMat (int dim);
void gauss (float **M, float *Y, int dim,float* s);
void AB (float ** M, float * Y,float * s, int dim, float * a, float * b,float * xd);
void  tri_sup(float **a, float *b, int dim) ;
float d(int j, float * xd);
void display (float ** M, float * Y,int dim);
void gplot (float *xd, float *yd, float xmin,float xmax ,float ymin,float ymax , int dim);
void spline (float * xd,float * yd,float * Y,float ** M,int dim);
int main (){
    printf("    trouver des points entre les données \n");
    // donnée
    int dim = 0;
    float * xd = NULL, *yd = NULL,  //  coordonnées des points
    *a = NULL, * b = NULL,          
    *s = NULL,                      // spline
    ** M = NULL , *Y = NULL;        // matrice a resoudre 

    //traitement
    getData(&dim,&xd,&yd,&a,&b,&s,&M,&Y);
    build(xd,yd,Y,M,dim);
    AB(M,Y,s,dim,a,b,xd);

   // gplot(xd,yd,0,6,-5,5,dim);

    // resultat
    displayResult(xd,dim);

    
    display(M,Y, dim);

    free(xd);free(yd);free(a);
    free(b);free(s);free(Y);
    for (int i = 0; i < dim; i++) {
        free(M[i]);
    }   
    
    return 0;
}        

float lagrange(float  x ,float * xd, float * yd, int dim ){
    float somme = 0 , produit; 
    int j  = 0,k = 0;
    for ( j = 0; j < dim; j++) {
        produit =1;
        for ( k = 0; k < dim; k++)    {
            if (j != k)  produit *= (x - xd[k])/(xd[j]-xd[k]);              
        }
        somme += yd[j]*produit;
    } 
    return somme;
}

void gplot (float *xd, float *yd, float xmin,float xmax ,float ymin,float ymax , int dim){
        FILE *GP = popen("gnuplot -persist","w");
        if(GP){
            //plot setup
            fprintf(GP,"set term wxt size 640 , 480\n" );
            fprintf(GP,"set title 'Interpollation' \n" );
            fprintf(GP, "set xlabel 'x'\n");
            fprintf(GP, "set ylabel 'y'\n");
            fprintf(GP,"set xzeroaxis\n" );
            fprintf(GP,"set xrange [%f:%f]\n",xmin,xmax );
            fprintf(GP,"set yrange [%f:%f]\n",ymin, ymax );
            /* lineare */
            //put data in a reusable internal data blocks
            fprintf(GP,"$data <<  EOF\n");
            for(int i = 0 ; i < dim ;i++){
                fprintf(GP,"%f %f\n",xd[i],yd[i]);   //data
            }
            fprintf(GP,"EOF\n");

            /* Lagrange */
            fprintf(GP,"$data1 << EOF\n");
            for (double x = xmin; x <= xmax;x+=0.01){
                fprintf(GP,"%f %f\n",x,lagrange(x,xd, yd, dim));  
            }
            fprintf(GP,"EOF\n");

            /* spline */
            /* fprintf(GP,"$data2 << EOF\n");
            int np = 100;
            float x = xmin, dx = (xmax-xmin)/(np-1);
            x -= dx;
            for (int i = xmin; i <= xmax;i+0.01){
                x += dx;
                fprintf(GP,"%f %f\n",x,lagrange(x,xd, yd, dim));  
            }  
            fprintf(GP,"EOF\n"); */

            //plot now
            fprintf(GP,  "plot '$data' using 1:2 w linespoints lc 'black' pt 7 title 'Linear', \
              '$data1' using 1:2 w lines lc 'blue' title 'Quadratic'\n");


            // send commands to gnuplot and close pipe
            fflush(GP);
            pclose(GP);
            
        }
        else printf("gnuplot not found ...\n ");
    }
float d(int j, float * xd){             // calcul de l' equidistance entre 2 points
    return   xd[j+1]- xd[j];
}
void gauss (float **a, float *b, int dim,float* x)  {
    tri_sup(a,b,dim);
    int n = dim;
    int i = n - 1 ;
    x[n-1] =  b[n-1]/ a[n-1][n-1];
    for(i = n -2;   0 <= i ; i--){
        float k = 0.0;
        for(int j = i+1; j<= n -1  ;j++){
            k += a[i][j] * x[j];
        } 
        x[i] = (b[i] - k)/a[i][i];
    }
}

void  tri_sup(float **a, float *b, int dim) {
    int n = dim;
    for (int k = 0; k <= n-1; k++) {
        for (int i = k + 1; i <= n-1 ; i++) {
            for (int j = k + 1; j <= n -1; j++) {
                a[i][j] = a[i][j] - (a[i][k] / a[k][k]) * a[k][j];
            }
            b[i] = b[i] - (a[i][k] / a[k][k]) * b[k];
            a[i][k] = 0;
        }
    }
}
void AB (float ** M, float * Y,float * s, int dim, float * a, float * b,float * xd){
    gauss (M,Y,dim,s);
    for (int j = 0; j < dim; j++) {
        a[j] = (Y[j]/d(j,xd)) - s[j] * (d(j,xd)/6);
        b[j] = Y[j+1]/d(j,xd) - s[j+1] * (d(j,xd)/6);
        printf("%f\t   %f\n",a[j],b[j]);
    }
}
void spline (float * xd,float * yd,float * Y,float ** M,int dim){
    float pj = 0;  float gama = d(0 ,xd)/(d( 0-1,xd)+d(0, xd));      // initialisation 
    M[0][0] = 2; M[0][1] = gama;
    for (int j = 1;  j < dim; j ++){
        gama = d(j ,xd) /  (d( j-1,xd)+d(j, xd));
        pj = 1 - gama;
        Y[j] = (6 / (d(j-1,xd)+d(j,xd))) * ((yd[j+1]-yd[j])/d(j,xd)  - ( yd[j]-yd[j-1])/d(j-1,xd)) ;
        for (int i = 0; i < dim; i++) {
            if (i == j+1) M[j][i] = gama;
            if (i == j) M [i][i] = 2;   
            if (i == j-1) M[j][i] = pj;                     
        }
    } 
}

void getData (int *dim, float ** xd, float ** yd, float ** a, float ** b, float ** s, float *** M, float ** Y){
    float *xx = NULL, *yy = NULL , * aa = NULL, * bb= NULL, * ss = NULL , ** m = NULL, *y = NULL;
    FILE * pf =  NULL ; // pointeur de fichier
    pf = fopen("data.txt","r"); // ouvrir le fichier et le mode
    if (pf != NULL){
        fscanf(pf,"%d",dim);    // get dimention
        xx = newVect(*dim);   yy = newVect (*dim);  ss = newVect(*dim); 
        aa = newVect(*dim);   bb = newVect(*dim);   m = newMat(*dim); y = newVect(*dim);
        for(int i = 0 ;   i < *dim ; i++){
            fscanf(pf,"%f",&xx[i]);         // get les data des points    
            fscanf(pf,"%f",&yy[i]);
        }
        for (int i = 0; i < *dim; i++) {        //initialisation 
            aa[i] = 0;bb[i] = 0;ss[i] = 0;
        }  
        for (int i = 0; i < *dim ; i++){        // initialisationde la matrice
            for (int j = 0; j < *dim; j++) {
                m[i][j] = 0;
            }
        }
        
    }else{
        problem("cant open file");
        exit(2);
    }
    // renvois les address 
    *xd = xx; *b = bb;
    *yd = yy;  *s = ss;
    *a = aa; *M = m; *Y = y;
}

float * newVect (int dim){       // allouer ligne
    float* v = NULL;
    v = malloc(dim* sizeof(float));
    if(v == NULL)  problem ("Ne peut allouer un vecteur !!!");
    return v;
}
void problem (char * message){
    printf ("%s",message);
    exit(2);
}
float **newMat (int dim){       // alouer colonne
    float ** A = NULL;
    A = malloc (dim * sizeof(float*));
    if(A == NULL)  problem ("Ne peut allouer un vecteur");
    for(int i = 0 ;   i < dim ; i++){
        A[i] = malloc(dim * sizeof(float));
        if(A[i] == NULL) problem ("Ne peut allouer une ligne de  matrice");
    }
    return A;
}
void displayResult ( float * x,int dim){
    printf("    les solutions du systeme : \n");
    for (int i = 0; i < dim; i++) {
        printf("\tx%i: %.2f\n",i+1 ,x[i]);
    }
}
void display (float ** M, float * Y,int dim){
    for (int i = 0; i < dim; i++) {
        for (int j = 0; j < dim; j++) {
            printf("%.2f\t ", M[i][j]);
        }
        printf("%.2f ", Y[i]);
        printf("\n");
    }
}